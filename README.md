<h1 align="center">AOA 👋, I'm JAWAD ANWAR </h1>
<h3 align="center">Software Engineer | FrontEnd Developer</h3>
   

<p align="left"> <a href="https://www.instagram.com/jawad.khokhar_?igsh=bmdqMHRyaDV1MzRs" target="blank"><img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" alt="Jawad Anwar Instagram" /></a> </p>

<img align="right" alt="Coding" width="400" src="https://raw.githubusercontent.com/devSouvik/devSouvik/master/gif3.gif">

- 🌱 I’m currently learning **ANGULAR**

- 📫 How to reach me **jawadanwar1122@gmail.com**

<!-- Jawad Anwar-->



<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://x.com/JawadKh93186850?t=E6YolghK7bF8ebscxl3KDg&s=09" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="Jawad Twitter" height="30" width="40" /></a>
<a href="https://www.linkedin.com/in/jawadanwar1122/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="Jawad Linkedin" height="30" width="40" /></a>

<a href="https://www.facebook.com/profile.php?id=100007635704891&mibextid=ZbWKwL" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="Jawad FaceBook" height="30" width="40" /></a>
<a href="https://www.instagram.com/jawad.khokhar_?igsh=bmdqMHRyaDV1MzRs" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="Jawad Instagram" height="30" width="40" /></a>

</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> 

 <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a>
  <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> 
  <a href="https://firebase.google.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/> </a> 
  <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a>
   <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a>
    <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg" alt="TypeScript" width="40" height="40"/> </a> 
    
   <a href="https://angular.io/" target="_blank" rel="noreferrer">
  <img src="https://angular.io/assets/images/logos/angular/angular.svg" alt="Angular" width="40" height="30"/>
</a>

  


     @jawadanwar

      

